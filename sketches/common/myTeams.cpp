#include "myTeams.h"

Team team_purple = {
    "BEARS",
    (MY_PINS)MY_PINS::Team_Purple,
    (char)184,
    (char)0,
    (char)184,
    0b0000001,
};

Team team_blue = {
    "N/A",
    MY_PINS::Team_Blue,
    (char)0,
    (char)0,
    (char)255,
    0b0000010,
};

Team team_green = {
    "SNAKES",
    MY_PINS::Team_Green,
    (char)0,
    (char)255,
    (char)0,
    0b0000100,
};

Team team_red = {
    "N/A",
    MY_PINS::Team_Red,
    (char)255,
    (char)0,
    (char)0,
    0b0001000,
};

Team team_orange = {
    "N/A",
    MY_PINS::Team_Orange,
    (char)255,
    (char)25,
    (char)0,
    0b0010000,
};

Team team_gold = {
    "LYNX",
    MY_PINS::Team_Gold,
    (char)255,
    (char)47,
    (char)5,
    0b0100000,
};

Team team_pink = {
    "MERCS",
    MY_PINS::Team_Pink,
    (char)255,
    (char)20,
    (char)113,
    0b1000000,
};

Team team_ADMIN = {
    "NEUTRAL",
    MY_PINS::team_ADMIN,
    (char)255,
    (char)255,
    (char)255,
    0b10000000,
};

Team& getTeam(MY_PINS pin) {
    switch (pin)
    {
    case MY_PINS::Team_Purple:
        return team_purple;

    case MY_PINS::Team_Blue:
        return team_blue;

    case MY_PINS::Team_Green:
        return team_green;

    case MY_PINS::Team_Red:
        return team_red;

    case MY_PINS::Team_Orange:
        return team_orange;

    case MY_PINS::Team_Gold:
        return team_gold;

    case MY_PINS::Team_Pink:
        return team_pink;

    case MY_PINS::team_ADMIN:
        return team_ADMIN;

    default:
        break;
    }
}

Team& getTeamByCRC(byte crc) {
    if (crc & team_purple.crc){
        return team_purple;
    }
    if (crc & team_blue.crc){
        return team_blue;
    }
    if (crc & team_green.crc){
        return team_green;
    }
    if (crc & team_red.crc){
        return team_red;
    }
    if (crc & team_orange.crc){
        return team_orange;
    }
    if (crc & team_gold.crc){
        return team_gold;
    }
    if (crc & team_pink.crc){
        return team_pink;
    }
    if (crc & team_ADMIN.crc){
        return team_ADMIN;
    }
}
