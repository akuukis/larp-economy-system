#ifndef MODULE_MY_VIEW
#define MODULE_MY_VIEW

#include "myLCD.h"
#include "myFrame.h"
#include "mySpinner.h"
#include "myHelpers.h"
#include "ThroneCamp.hpp"

#if defined(EPOXY_DUINO)
#include "pgmspace.h"
#else
#include <avr/pgmspace.h>
#endif

typedef char view_line[17];

#define VT(NAME, LINE) \
const char VT_##NAME[17] PROGMEM = LINE;

#define assign_VT(NAME, LOCAL) \
char LOCAL[17];\
strcpy_P(LOCAL, VT_##NAME)

#define use_VT(NAME) assign_VT(NAME, NAME)


VT(BLANK,          "                ");

VT(NOT,            "!      Not     !");
VT(ALREADY,        "!    Already   !");
VT(READY,          "!     ready    !");
VT(OWNED,          "!     owned    !");
VT(ALR_CAPTURING,  "!   capturing  !");

// Error when collecting
VT(NOTHING,        "!    Nothing   !");
VT(TO_COLLECT,     "!  to collect  !");

// Error when looting
VT(RECENTLY,       "!   Recently   !");
VT(REC_LOOTED,     "!    looted    !");

// Error when collecting
VT(CAPTURE_IN,     "!  Capture in  !");
VT(LIBERATION_IN,  "!  Liberate in !");
VT(PROGRESS,       "!   progress   !");

// Progress view
VT(LIBERATE_DELAY, "Liberating:     ");
VT(CAPTURE_DELAY,  "Capturing:      ");
VT(COLLECT_DELAY,  "Collect         ");
VT(LOOT_DELAY,     "Loot            ");
VT(UPGRADE_DELAY,  "Upgrading:      ");
VT(CAPTURE_PROG,   "!   Capturing  !");
VT(LIBERATE_PROG,  "!  Liberating  !");
VT(EEPROM_WARN,    "Clr EE in:      ");

VT(UPGRADE_CODE,   "UPGRADE CODE:   ");

VT(NOT_STARTED,    "GAME NOT STARTED");
VT(CAMP_LOOTED,    "   CAMP LOOTED  ");
VT(STARTS_IN,      "STARTS IN       ");
VT(READY_IN,       "READY IN        ");
VT(COLLECT_READY,  "Ready in        ");

VT(ACTION,         "#    Action    #");
VT(CANCELLED,      "#   cancelled  #");

VT(LIBERATING,     "#  Liberating  #");
VT(CAPTURING,      "#   Capturing  #");
VT(CAPTURED,       "#   Captured   #");
VT(BY,             "#  by          #");

VT(COLLECTED,      "#  Collected:  #");

VT(LOOTED,         "# Camp looted! #");
VT(LOOTED_VALUE,   "# Looted:      #");

VT(CODE_ALR_USED,  "#Code Alr. Used#");
VT(CAMP_UPGRADED,  "#Camp upgraded!#");
VT(WRONG_UPGRADE,  "# Wrong upgrade#");
VT(WRONG_CODE,     "#    Code      #");
VT(CODE_REW,       "#Rew:          #");

VT(SELECTED_TEAM,  "SELECTED TEAM:  ");
VT(OWNER,          "Owner:          ");
VT(CAPTURER,       "Cap.:           ");
VT(STOCK,          "Stock:          ");
VT(PLUS_1_EVERY,   "+1 every        ");
VT(LOOTING,        "Looting are we? ");
VT(EXPANSIONS,     "Upgrades: 0/2   ");

VT(SETUP_MENU_1,   "1.----  2.uptime");
VT(SETUP_MENU_2,   "3.type 4.EE.clr ");
VT(CLOCK,          "Clock:          ");
VT(BIRTH,          "Birth:          ");
VT(ID_RESC,        "Id:   Resc:     ");
VT(TYPE,           "Type:           ");

VT(EEPROM_CLEAR,   "#EEPROM Cleared#");
VT(PLEASE_RESET,   "#Please restart#");

VT(FIX_MENU_1,     "1.capt. 2.coll. ");
VT(FIX_MENU_2,     "3.loot  4.upgr. ");
VT(INSTA_CAPTURE,  "Insta Capture   ");
VT(LOOT,           "Loot:           ");
VT(PRODUCE,        "Prd:            ");

VT(OWNER_SAVE,     "# Owner:       #");
VT(STOCK_SAVE,     "# Stock:       #");
VT(LOOT_SAVE,      "# Loot:        #");
VT(SAVED,          "# saved!       #");
VT(UPGRADE_SET,    "#Upg code      #");

VT(TECH_MENU_1,    "Pulse: 1. Status");
VT(TECH_MENU_2,    "2.Type 3.Period ");
VT(PULSE_STATE,    "Pulse state:    ");
VT(PULSE_NO,       "NoStatus        ");
VT(PULSE_HT,       "HypothesisType  ");
VT(PULSE_HB,       "HypothesisBisect");
VT(PULSE_STRESS,   "Stress          ");
VT(PULSE_DONE,     "Done            ");
VT(PULSE_TYPE,     "Pulse type:     ");
VT(PULSE_TYPE_NO,  "NoType          ");
VT(PULSE_TYPE_R,   "Resistor        ");
VT(PULSE_TYPE_B,   "Both            ");
VT(PULSE_INTER,    "Pulse interval: ");

VT(VIEW,           "##### View #####");
VT(VIEW_DEFAULT,   "# Default      #");
VT(VIEW_SETUP,     "# Setup        #");
VT(VIEW_FIX,       "# Quick Fixes  #");
VT(VIEW_TECH,      "# Technical    #");

class MyView {
    public:
        MyView(MyLCD &lcd, MyFrame &frame, long default_notification_period);

        // Notification management
        bool has_active_notification();
        void render_notification();
        void reset_notification_alarm();


        // View template declarations

        void idle(const char* name, long time, long collect_countdown);

        void attempt_error(ThroneCampStatus ThroneCampStatusCode);

        void liberation_cooldown_bar(unsigned long remaining_time, unsigned long interval);
        void capture_cooldown_bar(unsigned long remaining_time, unsigned long interval);
        void collect_cooldown_bar(unsigned long remaining_time, unsigned long interval, byte reward, const char* resource);
        void loot_cooldown_bar(unsigned long remaining_time, unsigned long interval, byte reward, const char* resource);
        void upgrade_cooldown_bar(unsigned long remaining_time, unsigned long interval);
        void capture_progress_bar(unsigned long remaining_time, unsigned long interval);
        void liberation_progress_bar(unsigned long remaining_time, unsigned long interval);
        void clear_eeprom_safety_bar(unsigned long remaining_time, unsigned long interval);

        void upgrade_code_input(const char* input);
        void game_not_started(long remaining_time);
        void camp_is_looted(long remaining_time);

        void team_selected(const char* team);
        void capture_status(const char* owner_name, const char* capturer_name);
        void collect_status(int amount, const char* resource);
        void loot_status();
        void upgrade_status(byte count, long rate);

        void setup_menu();
        void setup_uptime(long clocktime, long uptime);
        void setup_type(int id, const char* resource, const char* name);

        void fix_menu();
        void fix_capture(const char* owner);
        void fix_stock(int amount, const char* resource);
        void fix_loot(long loot_time, long produce_time);
        void fix_upgrade(char upgrades);

        void technical_menu();
        void technical_pulse_state(PulseStatus pulse_state);
        void technical_pulse_type(PulseType pulse_type);
        void technical_pulse_interval(long interval);


        // Notification template declerations

        void notify_liberating(const char* team_name, long period=0);
        void notify_capturing(const char* team_name, long period=0);
        void notify_captured(const char* team_name, long period=0);
        void notify_collected(int resource_value, long period=0);
        void notify_looted(int resource_value, long period=0);

        void notify_wrong_upgrade_code(long period=0);
        void notify_upgrade_already_used(const char* upgrade_name, long period=0);
        void notify_camp_upgraded(const char* upgrade_name, long period=0);

        void notify_action_cancelled(long period=0);

        void notify_eeprom_cleared(long period=0);
        void notify_owner_saved(const char* owner_name, long period=0);
        void notify_stock_saved(long stock_value, long period=0);
        void notify_loot_saved(long loot_value, long period=0);
        void notify_upgrade_code_set(byte code, const char* code_value, long period=0);

        void notify_default_view(long period=0);
        void notify_setup_view(long period=0);
        void notify_fixes_view(long period=0);
        void notify_technical_view(long period=0);
    private:
        MyLCD &_lcd;
        MyFrame &_frame;
        char _notification[2][17];
        long _notification_alarm;
        long _default_notification_period;

        void _notify(const char* line1, const char* line2, long period);
};

#endif
