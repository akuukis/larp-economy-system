#line 2 "myViewTest"

#include <Arduino.h>
#include <AUnit.h>
#include "myView.h"

using namespace aunit;

const long DEFAULT_NOTIFICATION_PERIOD = 1000;

class myViewFixture: public TestOnce {
    public:
        MyLCD lcd;
        MyFrame frame = MyFrame(20);
        MyView view = MyView(lcd, frame, DEFAULT_NOTIFICATION_PERIOD);
};

testF(myViewFixture, notifications) {
    assertEqual(view.has_active_notification(), false);

    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "                \n"
      "                \n"
    );

    view.notify_action_cancelled();
    assertEqual(view.has_active_notification(), true);
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "#    Action    #\n"
      "#   cancelled  #\n"
    );

    for (int i=0; i<(DEFAULT_NOTIFICATION_PERIOD / 1000 * 20); i++) {
        frame.next();
    }
    assertEqual(view.has_active_notification(), false);

    view.notify_action_cancelled();
    assertEqual(view.has_active_notification(), true);
    view.reset_notification_alarm();
    assertEqual(view.has_active_notification(), false);
}

testF(myViewFixture, idle) {
    view.idle("Camp", ((3*60 + 20)*60 + 15)*1000, 0);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Camp    03:20:15\n"
      "                \n"
    );

    view.idle("Camp", ((3*60 + 20)*60 + 5)*1000, (59*60 + 20)*1000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Camp    03:20:05\n"
      "Ready in 59m20s \n"
    );
}

testF(myViewFixture, attempt_error) {
    view.attempt_error(ThroneCampStatus::NotReady);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "!      Not     !\n"
      "!     ready    !\n"
    );

    view.attempt_error(ThroneCampStatus::NotOwned);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "!      Not     !\n"
      "!     owned    !\n"
    );

    view.attempt_error(ThroneCampStatus::AlreadyOwned);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "!    Already   !\n"
      "!     owned    !\n"
    );

    view.attempt_error(ThroneCampStatus::NothingToCollect);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "!    Nothing   !\n"
      "!  to collect  !\n"
    );

    view.attempt_error(ThroneCampStatus::RecentlyLooted);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "!   Recently   !\n"
      "!    looted    !\n"
    );

    view.attempt_error(ThroneCampStatus::AlradyCapturing);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "!    Already   !\n"
      "!   capturing  !\n"
    );

    view.attempt_error(ThroneCampStatus::CaptureImprogress);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "!  Capture in  !\n"
      "!   progress   !\n"
    );

    view.attempt_error(ThroneCampStatus::LiberationImprogress);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "!  Liberate in !\n"
      "!   progress   !\n"
    );

}

testF(myViewFixture, liberation_cooldown_bar) {
    view.liberation_cooldown_bar(5000 - 1050, 5000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Liberating: 4s  \n"
      "|==*           |\n"
    );
}

testF(myViewFixture, capture_cooldown_bar) {
    view.capture_cooldown_bar(5000 - 1050, 5000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Capturing: 4s   \n"
      "|==+           |\n"
    );
}

testF(myViewFixture, collect_cooldown_bar) {
    view.collect_cooldown_bar(5000 - 1050, 5000, 45, "Food");
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Collect 45 Food \n"
      "|==*           |\n"
    );
}

testF(myViewFixture, loot_cooldown_bar) {
    view.loot_cooldown_bar(5000 - 1050, 5000, 1, "Food");
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Loot 1 Food     \n"
      "|==#           |\n"
    );
}

testF(myViewFixture, upgrade_cooldown_bar) {
    view.upgrade_cooldown_bar(5000 - 1050, 5000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Upgrading: 4s   \n"
      "|==#           |\n"
    );
}

testF(myViewFixture, capture_progress_bar) {
    view.capture_progress_bar(5000 - 1050, 5000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "!   Capturing  !\n"
      "|==+           |\n"
    );
}

testF(myViewFixture, liberation_progress_bar) {
    view.liberation_progress_bar(5000 - 1050, 5000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "!  Liberating  !\n"
      "|==#           |\n"
    );
}

testF(myViewFixture, clear_eeprom_safety_bar) {
    view.clear_eeprom_safety_bar(5000 - 1050, 5000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Clr EE in: 4s   \n"
      "|==*           |\n"
    );
}


testF(myViewFixture, upgrade_code_input) {
    view.upgrade_code_input("");
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "UPGRADE CODE:   \n"
      "                \n"
    );

    view.upgrade_code_input("abcdefghlmn");
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "UPGRADE CODE:   \n"
      "abcdefghlmn     \n"
    );
}

testF(myViewFixture, game_not_started) {
    view.game_not_started((7*60 + 23)*1000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "GAME NOT STARTED\n"
      "STARTS IN 7m24s \n"
    );
}

testF(myViewFixture, camp_is_looted) {
    view.camp_is_looted((7*60 + 23)*1000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "   CAMP LOOTED  \n"
      "READY IN 7m24s  \n"
    );
}


testF(myViewFixture, team_selected) {
    view.team_selected("Blue");
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "SELECTED TEAM:  \n"
      "Blue            \n"
    );
}

testF(myViewFixture, capture_status) {
    view.capture_status("Blue", "Pink");
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Owner: Blue     \n"
      "Cap.: Pink      \n"
    );
}

testF(myViewFixture, collect_status) {
    view.collect_status(123, "Stuff", (25*60 + 5)*1000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Stock:123Stuff  \n"
      "+1 every 25m5s  \n"
    );
}

testF(myViewFixture, loot_status) {
    view.loot_status();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Looting are we? \n"
      "                \n"
    );
}

testF(myViewFixture, upgrade_status) {
    view.upgrade_status(0);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Upgrades: 0/2   \n"
      "                \n"
    );

    view.upgrade_status(1);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Upgrades: 1/2   \n"
      "                \n"
    );

    view.upgrade_status(2);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Upgrades: 2/2   \n"
      "                \n"
    );

    view.upgrade_status(3);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Upgrades: 3/2   \n"
      "                \n"
    );
}


testF(myViewFixture, setup_menu) {
    view.setup_menu();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "1.----  2.uptime\n"
      "3.type 4.EE.clr \n"
    );
}

testF(myViewFixture, setup_uptime) {
    view.setup_uptime((33*60 + 3)*1000, ((2*60 + 55)*60 + 12)*1000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Clock:  00:33:03\n"
      "Birth:  02:55:12\n"
    );
}

testF(myViewFixture, setup_type) {
    view.setup_type(3, "Food", "Farm");
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Id:3  Resc:Food \n"
      "Type: Farm      \n"
    );
}


testF(myViewFixture, fix_menu) {
    view.fix_menu();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "1.capt. 2.coll. \n"
      "3.loot  4.upgr. \n"
    );
}

testF(myViewFixture, fix_capture) {
    view.fix_capture("Blue");
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Insta Capture   \n"
      "Owner: Blue     \n"
    );
}

testF(myViewFixture, fix_stock) {
    view.fix_stock(123, "Food");
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Stock:123Food   \n"
      "                \n"
    );
}

testF(myViewFixture, fix_loot) {
    view.fix_loot(12*1000, 55*1000);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Loot: 12s       \n"
      "Prd: 55s        \n"
    );
}

testF(myViewFixture, fix_upgrade) {
    view.fix_upgrade(0b1101);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "                \n"
      "UAUU            \n"
    );
}


testF(myViewFixture, technical_menu) {
    view.technical_menu();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Pulse: 1. Status\n"
      "2.Type 3.Period \n"
    );
}

testF(myViewFixture, technical_pulse_state) {
    view.technical_pulse_state(PulseStatus::NoStatus);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Pulse state:    \n"
      "NoStatus        \n"
    );

    view.technical_pulse_state(PulseStatus::Hypothesis_Type);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Pulse state:    \n"
      "HypothesisType  \n"
    );

    view.technical_pulse_state(PulseStatus::Hypothesis_Bisect);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Pulse state:    \n"
      "HypothesisBisect\n"
    );

    view.technical_pulse_state(PulseStatus::Stress);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Pulse state:    \n"
      "Stress          \n"
    );

    view.technical_pulse_state(PulseStatus::Done);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Pulse state:    \n"
      "Done            \n"
    );
}

testF(myViewFixture, technical_pulse_type) {
    view.technical_pulse_type(PulseType::NoType);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Pulse type:     \n"
      "NoType          \n"
    );

    view.technical_pulse_type(PulseType::Resistor);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Pulse type:     \n"
      "Resistor        \n"
    );

    view.technical_pulse_type(PulseType::Both);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Pulse type:     \n"
      "Both            \n"
    );
}

testF(myViewFixture, technical_pulse_interval) {
    view.technical_pulse_interval(1234);
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "Pulse interval: \n"
      "1234 ms         \n"
    );
}


testF(myViewFixture, notify_liberating) {
    view.notify_liberating("Blue");
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "#  Liberating  #\n"
      "#  by Blue     #\n"
    );
}

testF(myViewFixture, notify_capturing) {
    view.notify_capturing("Blue");
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "#   Capturing  #\n"
      "#  by Blue     #\n"
    );
}

testF(myViewFixture, notify_captured) {
    view.notify_captured("Blue");
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "#   Captured   #\n"
      "#  by Blue     #\n"
    );
}

testF(myViewFixture, notify_collected) {
    view.notify_collected(1234);
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "#  Collected:  #\n"
      "#     1234     #\n"
    );
}

testF(myViewFixture, notify_looted) {
    view.notify_looted(1234);
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "# Camp looted! #\n"
      "# Looted: 123  #\n"
    );
}


testF(myViewFixture, notify_wrong_upgrade_code) {
    view.notify_wrong_upgrade_code();
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "# Wrong upgrade#\n"
      "#    Code      #\n"
    );
}

testF(myViewFixture, notify_upgrade_already_used) {
    view.notify_upgrade_already_used("FASHION");
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "#Code Alr. Used#\n"
      "#Rew: FASHION  #\n"
    );
}

testF(myViewFixture, notify_camp_upgraded) {
    view.notify_camp_upgraded("FASHION");
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "#Camp upgraded!#\n"
      "#Rew: FASHION  #\n"
    );
}


testF(myViewFixture, notify_action_cancelled) {
    view.notify_action_cancelled();
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "#    Action    #\n"
      "#   cancelled  #\n"
    );
}


testF(myViewFixture, notify_eeprom_cleared) {
    view.notify_eeprom_cleared();
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "#EEPROM Cleared#\n"
      "#Please restart#\n"
    );
}

testF(myViewFixture, notify_owner_saved) {
    view.notify_owner_saved("Blue");
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "# Owner: Blue  #\n"
      "# saved!       #\n"
    );
}

testF(myViewFixture, notify_stock_saved) {
    view.notify_stock_saved(1234);
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "# Stock: 1234  #\n"
      "# saved!       #\n"
    );
}

testF(myViewFixture, notify_loot_saved) {
    view.notify_loot_saved(1234);
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "# Loot: 1234   #\n"
      "# saved!       #\n"
    );
}

testF(myViewFixture, notify_upgrade_code_set) {
    view.notify_upgrade_code_set(1, "HOTSHOE-FASHION");
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "#Upg code 1 set#\n"
      "#HOTSHOE-FASHIO#\n"
    );
}


testF(myViewFixture, notify_default_view) {
    view.notify_default_view();
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "##### View #####\n"
      "# Default      #\n"
    );
}

testF(myViewFixture, notify_setup_view) {
    view.notify_setup_view();
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "##### View #####\n"
      "# Setup        #\n"
    );
}

testF(myViewFixture, notify_fixes_view) {
    view.notify_fixes_view();
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "##### View #####\n"
      "# Quick Fixes  #\n"
    );
}

testF(myViewFixture, notify_technical_view) {
    view.notify_technical_view();
    view.render_notification();
    lcd.flush();
    assertEqual(
      lcd.serizalizeScreen(),
      "##### View #####\n"
      "# Technical    #\n"
    );
}

//---------------------------------------------------------------------------

void setup() {
    // lcd.init();
    Serial.begin(9600);
}

void loop() {
    TestRunner::run();
}
