#include "myEEPROM.h"

void MyEEPROM::clean() const {
    for (int i = 0; i < EEPROM.length(); i++) {
        EEPROM.update(i, 0);
    }
}

void MyEEPROM::putHeartbeat(short beat) const {
    EEPROM.put((int)MY_ADDRESS::Heartbeat, beat);
}

short MyEEPROM::getHeartbeat() const {
    short beat;
    EEPROM.get((int)MY_ADDRESS::Heartbeat, beat);
    return beat;
}

void MyEEPROM::putPile(short pile) const {
    EEPROM.put((int)MY_ADDRESS::BD_PILE, pile);
}

short MyEEPROM::getPile() const {
    short pile;
    EEPROM.get((int)MY_ADDRESS::BD_PILE, pile);
    return pile;
}

void MyEEPROM::putId(unsigned char id) const {
    EEPROM.put((int)MY_ADDRESS::Camp_Id, id);
}

unsigned char MyEEPROM::getId() const {
    unsigned char id;
    EEPROM.get((int)MY_ADDRESS::Camp_Id, id);
    return id;
}

void MyEEPROM::putPulse(Pulse pulse) const {
    EEPROM.put((int)MY_ADDRESS::Power_Pulse, pulse);
}

Pulse MyEEPROM::getPulse() const {
    Pulse pulse;
    EEPROM.get((int)MY_ADDRESS::Power_Pulse, pulse);
    return pulse;
}

void MyEEPROM::appendLog(LogEntry log) const {
    short log_count = getLogCount();
    EEPROM.put((int)MY_ADDRESS::Logs + 2 + log_count * 6, log);
    EEPROM.put((int)MY_ADDRESS::Logs, (short)log_count + 1);
}

LogEntry MyEEPROM::getLog(short index) const {
    LogEntry log;
    EEPROM.get((int)MY_ADDRESS::Logs + 2 + index * 6, log);
    return log;
}

short MyEEPROM::getLogCount() const {
    short log_count;
    EEPROM.get((int)MY_ADDRESS::Logs, log_count);
    return log_count;
}





