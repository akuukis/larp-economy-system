#include "mySpinner.h"

static byte spinnerCounter = 0;
const byte SPINNER_FRAME_COUNT = 12;
static char SPINNER_FRAMES[SPINNER_FRAME_COUNT+1] = "+++***###===";

char spinner() {
    spinnerCounter = (spinnerCounter + 1) % SPINNER_FRAME_COUNT;
    return SPINNER_FRAMES[spinnerCounter];
}

void progressBar(char* bar, unsigned long progress, unsigned long interval) {
  byte length = 14 * ((float)min(progress, interval) / interval);

  for (byte i=0; i<16; i++) {
      bar[i] = ' ';
  }

  bar[0] = '|';
  bar[15] = '|';
  bar[16] = 0;

  // Completed segments
  for (byte i=0; i<length; i++) {
    bar[i+1] = '=';
  }

  // In progress segment
  if (length < 14) {
      bar[length+1] = spinner();
  }
}
