#ifndef MODULE_MY_TEAMS
#define MODULE_MY_TEAMS

#include "myConfig.h"
#include <Arduino.h>

struct Team {
    const char* name;
    MY_PINS pin;
    char red;
    char green;
    char blue;
    byte crc;
};

extern Team team_purple;
extern Team team_blue;
extern Team team_green;
extern Team team_red;
extern Team team_orange;
extern Team team_gold;
extern Team team_pink;
extern Team team_ADMIN;

Team& getTeam (MY_PINS pin);
Team& getTeamByCRC(byte crc);

#endif
