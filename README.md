# larp-economy-system

## Deploy
1. Use `upload` task in VSCode tasks.
2. For production, uncomment line `// #define PRODUCTION` in `ThroneCamp.hpp` file.

## Testing
`./run-tests.sh`
`./update-snapshots.sh`
Running tests will generate .out files in 'tests' folder. They are copared against .snap files.
To write new tests edit 'tests/testCases.txt' run `./run-tests.sh` and check .out files manually and if the output is correct run `./update-snapshots.sh`.

WINDOWS 10
1. Install Git Bash
2. Register GitLab, clone this project with Git Bash.
3. Download VS code, activate all predefined settings.
4. Download arduino-cli
5. Download GNUWin32  to execute make commands on windows.
6. Execute copy `c:\MinGW\bin\mingw32-make.exe c:\MinGW\bin\make.exe`, to rename it to make
7. Add to windows system environment variables path to make.exe
8. Move arduino-cli to a proper place where to store executables. Like "C:\arduino-cli_0.7.0_Windows_64bit"
9. Add to windows system environment variables path to arduino-cli.exe
10. Create folder "C:\Users\<User>\AppData\Local\Arduino15"
11. Run `arduino-cli config init`
12. Create folder C:\Users\<User>\AppData\Local\Arduino15\packages
13. Run `arduino-cli core update-index`
14. Run `make boards` in larp-economy-system root folder
15. Create copy arduino-cli.yaml.example named arduino-cli.yaml
16. Change `data` path to: "C:\Users\<User>\AppData\Local\Arduino15"
17. Change `downloads` path to: "C:\Users\<User>\AppData\Local\Arduino15\staging"
18. Change `user` path to root of larp-economy-system
19. Run `make compile` should work now (probably in cmd some random errors)
20. Install python3, check the checkbox add python to path when installing
21. In C:\Users\<User>\AppData\Local\Programs\Python\Python38 create a copy of python.exe as python3.exe
22. Download msys2 and install it
23. Update msys2 with `pacman -Syu`
24. Execute `pacman -S base-devel gcc`
25. Add to windows system environment variables path `C:\msys64\mingw64\bin`
26. Add to windows system environment variables path `C:\msys64\usr\bin`
27. Execute `make test` from CMD (should be all OK)
28. Plugin the board using arduino hw-417 as a connector.
29. In Makefile change SERIAL_PORT to be the port where you plugged in the board. Check in Device Manager under ports.
30. Execute `make upload`
31. To get intelsense working create a copy of c_cpp_properties.json.example called c_cpp_properties.json
32. Change the paths of c_cpp_properties.json so they match with your system.


## Memory optimization
> It's too big already. Here's some places the memory has gone:

Program Storage Space / Global Variables:
- 1400 / 0 - `sprintf`.
- 900 / 150 - `Serial`.

Sources for tips:
- https://oscarliang.com/arduino-sketch-too-big-error-reduce-sketch-size/
- https://stackoverflow.com/a/52395977/4817809
- https://learn.adafruit.com/memories-of-an-arduino/optimizing-sram

Examples:
1. The first pass of `F("...")` moved ~700 bytes from dynamic memory to program storage space.
