#include "myLCD.h"

MyLCD::MyLCD() : LiquidCrystal_I2C(0x27, 16, 2) {
    _backlightOn = false;  // Assume it defaults to false..
    clear();
}

/**
 * Valid ranges:
 *     * x: [0, 15]
 *     * y: [0, 1]
 */
void MyLCD::printAt(byte x, byte y, const char* content) { /* content = "                " */
    for(byte i=0; i<16-x; i++) {
        if (content[i] == 0) break;
        _buffer[y][x+i] = content[i];
    }
}

void MyLCD::printAt(byte x, byte y, String content) {
    for(byte i=0; i<min((byte)(16-x), (byte)content.length()); i++) {
        _buffer[y][x+i] = content.charAt(i);
    }
}

void MyLCD::printDef(const char* content1, const char* content2){
    printAt(0,0,content1);
    printAt(0,1,content2);
}

void MyLCD::printDef(String content1, String content2){
    printAt(0,0,content1);
    printAt(0,1,content2);
}

void MyLCD::clear() {
    for (byte i=0; i<16; i++) {
        _buffer[0][i] = ' ';
        _buffer[1][i] = ' ';
    }
    _buffer[0][16] = 0;
    _buffer[1][16] = 0;
}

void MyLCD::flush() {
    setCursor(0, 0);
    print(_buffer[0]);
    setCursor(0, 1);
    print(_buffer[1]);
}

void MyLCD::noBacklight(void) {
    _backlightOn = false;
    LiquidCrystal_I2C::noBacklight();
}

void MyLCD::backlight(void) {
    _backlightOn = true;
    LiquidCrystal_I2C::backlight();
}

bool MyLCD::isBacklight(void) {
    return _backlightOn;
}
