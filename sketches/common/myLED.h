#ifndef MODULE_MY_LED
#define MODULE_MY_LED

#include "myConfig.h"
#include "myTeams.h"
#include <inttypes.h>
#include <Arduino.h>

class MyLED {
public:
    MyLED();
    void setTeamColor (Team team);
    void setAlternateTeamColors (Team team1, Team team2);
    void alternateColors ();
private:
    bool _is_alternating;
    char _alternate_colors[4][3];
    byte _alternate_cursor;
};

#endif
