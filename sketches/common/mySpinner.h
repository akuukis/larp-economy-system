#ifndef MODULE_MY_SPINNER
#define MODULE_MY_SPINNER

#include <Arduino.h>

char spinner();
void progressBar(char* bar, unsigned long progress, unsigned long interval);

#endif
