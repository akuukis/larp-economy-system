#include <Wire.h>
#include <EEPROM.h>

#include <myConfig.h>
#include <myEEPROM.h>
#include <myLCD.h>

static MyEEPROM eeprom;
static MyLCD lcd = MyLCD();

unsigned long lastCapture = 0;
MY_PINS team = MY_PINS::team_ADMIN;
unsigned long Captured[8] = {0, 0, 0, 0, 0, 0, 0, 0};

unsigned long Collected[8] = {0, 0, 0, 0, 0, 0, 0, 0};

unsigned long Looted[8] = {0, 0, 0, 0, 0, 0, 0, 0};

unsigned int Upgraded[8] = {0, 0, 0, 0, 0, 0, 0, 0};

void setup(){
    lcd.flush();
    lcd.clear();
    lcd.setBacklight(true);

    Serial.begin(9600);
    while (!Serial) continue;

    short count = eeprom.getLogCount();
    for (short i = 0; i < count; i++) {
        LogEntry log = eeprom.getLog(i);
        //Get captures totals
        if(log.type == EventType::Capture){
            Captured[(unsigned char)team - 1] += (log.heartbeat - lastCapture);
            team = log.team;
            lastCapture = log.heartbeat;
        }
        //Get collect totals
        if(log.type == EventType::Collect){
            Collected[(unsigned char)log.team - 1] += log.value;
        }
        //Get loot totals
        if(log.type == EventType::Loot){
            Looted[(unsigned char)log.team - 1] += 1;
        }
        //Get upgrade totals
        if(log.type == EventType::Upgrade){
            Upgraded[(unsigned char)log.team - 1] = Upgraded[(unsigned char)log.team - 1] ^ (0b0001<<log.value);
        }
    }
    
}

void loop(){
    Serial.println("Id: "+eeprom.getId());
    Serial.println("Captures:");
    for (int x=1;x<8;x++){
        Serial.println(String(x+1)+": "+String(Captured[x]));
    }
    Serial.println("Collects:");
    for (int x=1;x<8;x++){
        Serial.println(String(x+1)+": "+String(Collected[x]));
    }
    Serial.println("Loots:");
    for (int x=0;x<8;x++){
        Serial.println(String(x+1)+": "+String(Looted[x]));
        lcd.printAt(0,0,String(x+1)+": "+String(Looted[x]));
        delay(1000);   
    }
    Serial.println("Upgrades:");
    for (int x=1;x<8;x++){
        Serial.println(String(x+1)+": "+String(Upgraded[x]));
    }
    delay(5000);

    Serial.println("Log replay:");
    short count = eeprom.getLogCount();
    for (short i = 0; i < count; i++) {
        LogEntry log = eeprom.getLog(i);
        //Get captures totals
        if(log.type == EventType::Capture){
            Captured[(unsigned char)team - 1] += (log.heartbeat - lastCapture);
            team = log.team;
            lastCapture = log.heartbeat;
        }
        //Get collect totals
        if(log.type == EventType::Collect){
            Serial.println("HB: " + String(log.heartbeat) + " Team: " + String((int)(log.team)) + " Value: " + String(log.value));
        }
        //Get loot totals
        if(log.type == EventType::Loot){
            Looted[(unsigned char)log.team - 1] += 1;
        }
        //Get upgrade totals
        if(log.type == EventType::Upgrade){
            Upgraded[(unsigned char)log.team - 1] = Upgraded[(unsigned char)log.team - 1] ^ (0b0001<<log.value);
        }
    }
    delay(5000);
}