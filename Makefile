ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
BOARD_FQBN=arduino:avr:pro
ifeq ($(OS),Windows_NT)
	SERIAL_PORT=COM3
else
	SERIAL_PORT=/dev/ttyUSB0
endif
UNIT_TESTS := $(notdir $(wildcard tests/*))

test:
	set -e; \
	for dir in $(UNIT_TESTS); do \
		$(MAKE) -C tests/$$dir -j; \
	done; \
	echo -e "\n"; \
	for dir in $(UNIT_TESTS); do \
		echo '==== Running:' $$dir; \
		tests/$$dir/$$dir.out; \
		echo; \
	done

clean:
	set -e; \
	for dir in $(UNIT_TESTS); do \
		echo $$dir; \
		$(MAKE) -C tests/$$dir clean; \
	done

compile:
	arduino-cli compile --config-file arduino-cli.yaml --warnings default -b ${BOARD_FQBN} ./sketches/main

compile-analysis:
	arduino-cli compile --config-file arduino-cli.yaml --warnings default -b ${BOARD_FQBN} ./sketches/analysis

compile-validate:
	arduino-cli compile --config-file arduino-cli.yaml --warnings default -b ${BOARD_FQBN} ./sketches/validate_hardware

compile-selftest:
	arduino-cli compile --config-file arduino-cli.yaml --warnings default -b ${BOARD_FQBN} ./sketches/selftest

compile-clear:
	arduino-cli compile --config-file arduino-cli.yaml --warnings default -b ${BOARD_FQBN} ./sketches/clear_EEPROM

compile-logs:
	arduino-cli compile --config-file arduino-cli.yaml --warnings default -b ${BOARD_FQBN} ./sketches/analysis

upload: compile
	arduino-cli upload -p ${SERIAL_PORT} -b ${BOARD_FQBN} ./sketches/main

upload-analysis: compile-analysis
	arduino-cli upload -p ${SERIAL_PORT} -b ${BOARD_FQBN} ./sketches/analysis

upload-validate: compile-validate
	arduino-cli upload -p ${SERIAL_PORT} -b ${BOARD_FQBN} ./sketches/validate_hardware

upload-selftest: compile-selftest
	arduino-cli upload -p ${SERIAL_PORT} -b ${BOARD_FQBN} ./sketches/selftest

upload-clear: compile-clear
	arduino-cli upload -p ${SERIAL_PORT} -b ${BOARD_FQBN} ./sketches/clear_EEPROM

upload-logs: compile-logs
	arduino-cli upload -p ${SERIAL_PORT} -b ${BOARD_FQBN} ./sketches/analysis

boards:
	arduino-cli core install arduino:avr
	arduino-cli board listall

dependencies:
	arduino-cli lib install Low-Power@1.81.0
	arduino-cli lib install AUnit@1.6.1
	arduino-cli lib install "LiquidCrystal I2C"@1.1.2
	wget https://github.com/bxparks/EpoxyDuino/archive/refs/tags/v1.3.0.tar.gz -O libraries/EpoxyDuino.tar.gz
	mkdir libraries/EpoxyDuino
	tar -xaf ./libraries/EpoxyDuino.tar.gz -C ./libraries/EpoxyDuino --strip-components=1
	rm ./libraries/EpoxyDuino.tar.gz

dependencies-clean:
	rm -rf ./libraries/AUnit
	rm -rf ./libraries/Low-Power
	rm -rf ./libraries/LiquidCrystal_I2C
	rm -rf ./libraries/EpoxyDuino
