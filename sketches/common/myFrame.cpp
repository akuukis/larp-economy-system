#include "myFrame.h"

MyFrame::MyFrame(char target_frames_per_second) {
    _eeprom = MyEEPROM();
    _target_frames_per_second = target_frames_per_second;

    const short beat1 = _eeprom.getHeartbeat();
    const short beat2 = _eeprom.getLogCount() > 0 ? _eeprom.getLog(_eeprom.getLogCount()-1).heartbeat : -32768;
    _turned_on_ms = fromHeartbeat(max(beat1, beat2));
}

void MyFrame::setTurnedOn(long time_ms) {
    _turned_on_ms = time_ms;
    _eeprom.putHeartbeat(toHeartbeat(uptime_ms + _turned_on_ms));
}
long MyFrame::getTurnedOn() {
    return _turned_on_ms;
}

/**
 * Call at the start of each loop.
 *
 * Delays enough miliseconds to sustain a constant frame rate,
 * and sets `uptime_ms` and `prev_frame_ms` accordingly.
 *
 * If a frame happens to be longer than target fps implies,
 * it does not compensate for it during next frames.
 **/
void MyFrame::next() {
    const int busy_ms = millis() - uptime_ms;
    const int idle_ms = max(0, 1000 / _target_frames_per_second - busy_ms);
    if(idle_ms > 0) {delay(idle_ms);}

    prev_frame_ms = busy_ms + idle_ms;
    uptime_ms = uptime_ms + prev_frame_ms;

    clocktime_ms = uptime_ms + _turned_on_ms;
}

/**
 * @brief Returns `true` only once per `interval`.
 */
bool MyFrame::oncePerInterval(long interval, long offset) const {  /* offset = 0 */
    if(interval == 0 || (uptime_ms - offset) % interval < prev_frame_ms) {
        return true;
    } else {
        return false;
    }
}

void MyFrame::saveHeartbeat() const {
    _eeprom.putHeartbeat(getHeartbeat());
}
short MyFrame::getHeartbeat() const {
    return clocktime_ms / 1000;
}
short MyFrame::toHeartbeat(long time_ms) const {
    return time_ms / 1000;
}
long MyFrame::fromHeartbeat(short beat) const {
    return (long)beat * 1000;
}