#ifndef MODULE_MY_FRAME
#define MODULE_MY_FRAME

#include <Arduino.h>
#include "myEEPROM.h"

class MyFrame {
public:
    /**
     * 20 is the "human likes responsiveness" speed.
     */
    MyFrame(char target_frames_per_second);
    /**
     * The time at which I was turned on, negative being before the start of the game.
     */
    long getTurnedOn();
    void setTurnedOn(long time_ms);
    /**
     * The value of millis() at the point of last `next()`.
     */
    long uptime_ms = 0;
    /**
     * The time since start of the game. May be negative if game not started yet.
     */
    long clocktime_ms = 0;
    /**
     * The duration of the previous frame. They sum up to uptime_ms.
     */
    int prev_frame_ms = 0;
    void next();
    bool oncePerInterval(long interval, long offset = 0) const;

    void saveHeartbeat() const;
    short getHeartbeat() const;
    short toHeartbeat(long time_ms) const;
    long fromHeartbeat(short beat) const;
private:
    long _turned_on_ms = 0;
    char _target_frames_per_second;
    MyEEPROM _eeprom;
};

#endif
