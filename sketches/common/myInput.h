#ifndef MODULE_MY_PINS
#define MODULE_MY_PINS

#include "myConfig.h"
#include <Arduino.h>

struct Buttons {
    bool Team_Purple;
    bool Team_Blue;
    bool Team_Green;
    bool Team_Red;
    bool Team_Orange;
    bool Team_Gold;
    bool Team_Pink;
    MY_PINS Team;
    bool Action_1;
    bool Action_2;
    bool Action_3;
    bool Action_4;
    MY_PINS Action;
    int CRC_Team;
    int CRC_Action;
    int CRC;
};

class MyInput {
public:
    // @brief Values are `true` once at the start of push.
    Buttons keysDown;
    // @brief Values are `true` as long as pushed down.
    Buttons keysPress;
    // @brief Values are `true` once after the end of push.
    Buttons keysUp;
    MyInput();
    void read();
};

#endif