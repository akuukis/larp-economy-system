#include "myLED.h"

MyLED::MyLED() {
    pinMode((uint8_t)MY_PINS::LED_Red, OUTPUT);
    pinMode((uint8_t)MY_PINS::LED_Green, OUTPUT);
    pinMode((uint8_t)MY_PINS::LED_Blue, OUTPUT);
}

void MyLED::setTeamColor(Team team) {
    _is_alternating = false;
    analogWrite((uint8_t)MY_PINS::LED_Red, team.red);
    analogWrite((uint8_t)MY_PINS::LED_Green, team.green);
    analogWrite((uint8_t)MY_PINS::LED_Blue, team.blue);
}

void MyLED::setAlternateTeamColors(Team team1, Team team2) {
    _is_alternating = true;
    _alternate_cursor = 0;

    _alternate_colors[0][0] = team1.red;
    _alternate_colors[0][1] = team1.green;
    _alternate_colors[0][2] = team1.blue;

    _alternate_colors[1][0] = team1.red;
    _alternate_colors[1][1] = team1.green;
    _alternate_colors[1][2] = team1.blue;

    _alternate_colors[2][0] = team1.red;
    _alternate_colors[2][1] = team1.green;
    _alternate_colors[2][2] = team1.blue;

    _alternate_colors[3][0] = team2.red;
    _alternate_colors[3][1] = team2.green;
    _alternate_colors[3][2] = team2.blue;
}

void MyLED::alternateColors() {
    if (!_is_alternating) return;

    _alternate_cursor = (_alternate_cursor + 1) % 4;
    analogWrite((uint8_t)MY_PINS::LED_Red, _alternate_colors[_alternate_cursor][0]);
    analogWrite((uint8_t)MY_PINS::LED_Green, _alternate_colors[_alternate_cursor][1]);
    analogWrite((uint8_t)MY_PINS::LED_Blue, _alternate_colors[_alternate_cursor][2]);
}
