#line 2 "mySpinnerTest"

#include <Arduino.h>
#include <AUnit.h>
#include <EEPROM.h>
#include "myFrame.h"

using namespace aunit;

const byte FPS = 20;

class myFrameFixture: public TestOnce {
    protected:
        void setup() override {
            TestOnce::setup();

            for (int i = 0; i<EEPROM.length(); i++) {
                EEPROM.update(i, 0);
            }

            frame = new MyFrame(FPS);
            frame->next(); // Makes sure that public properties are initialised
        }

        void teardown() override {
            delete frame;
            TestOnce::teardown();
        }

        MyFrame* frame;
};

testF(myFrameFixture, getTurnedOn) {
    assertEqual(frame->getTurnedOn(), (long)0);
    frame->setTurnedOn(123456);
    assertEqual(frame->getTurnedOn(), (long)123456);
}

testF(myFrameFixture, matchesTargetFPS) {
    long before_ms = frame->uptime_ms;
    for (int i=0; i<FPS; i++) {
        frame->next();
    }
    long after_ms = frame->uptime_ms;

    assertNear(after_ms - before_ms, (long)1000, (long)50);
}

testF(myFrameFixture, oncePerInterval) {
    const long offset = frame->uptime_ms;

    assertEqual(frame->oncePerInterval(1000, offset), true);

    for (int i = 0; i<(FPS - 1); i++) {
        frame->next();
        assertEqual(frame->oncePerInterval(1000, offset), false);
    }

    frame->next();
    assertEqual(frame->oncePerInterval(1000, offset), true);
}

testF(myFrameFixture, getHeartbeat) {
    assertEqual(frame->getHeartbeat(), (short)(millis() / 1000));
}

testF(myFrameFixture, toHeartbeat) {
    assertEqual(frame->toHeartbeat(0), 0);
    assertEqual(frame->toHeartbeat(1000), 1);
}

testF(myFrameFixture, fromHeartbeat) {
    assertEqual(frame->fromHeartbeat(0), (long)0);
    assertEqual(frame->fromHeartbeat(1), (long)1000);
}

//---------------------------------------------------------------------------

void setup() {
    Serial.begin(9600);
}

void loop() {
    TestRunner::run();
}
