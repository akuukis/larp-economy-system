#ifndef MODULE_MY_PULSE
#define MODULE_MY_PULSE

#include "myConfig.h"
#include "myLCD.h"
#include "myEEPROM.h"
#include <Arduino.h>

class MyPulse : private Pulse {
public:
    MyPulse(MyLCD lcd);
    void drain();

    PulseStatus getStatus();
    void setStatus(PulseStatus Status);

    PulseType getType();
    void setType(PulseType Type);

    long getInterval();
    void setInterval(long Interval);
protected:
    void put();
    MyLCD _lcd;
    MyEEPROM _eeprom;
};

#endif
