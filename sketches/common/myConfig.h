#ifndef MODULE_MY_CONFIG
#define MODULE_MY_CONFIG

// Uncomment for Serial debug messages:
//#define USE_SERIAL

#define SHIFT 12

#include <inttypes.h>

enum class MY_PINS : unsigned char {
    Team_Purple = 8,
    Team_Blue = 7,
    Team_Green = 6,
    Team_Red = 5,
    Team_Orange = 4,
    Team_Gold = 3,
    Team_Pink = 2,
    team_ADMIN = 1,

    LED_Red = 9,
    LED_Green = 10,
    LED_Blue = 11,

    Power_Sink = 12,
    Onboard_LED = 13,  // Hardwired on board.

    Action_1 = 17,
    Action_2 = 16,
    Action_3 = 15,
    Action_4 = 14,
};

/**
 * Our memory space is [0, 1023].
 */
enum class MY_ADDRESS : short int {
    //šajā adresē glabājas heartbeat, kas ir heartbeat/8 ; Varētu likt līdz 767 adresei pie tagad settingiem
    Heartbeat = 0 + SHIFT,
    //2 byte backdoor pile adrese. Arī izmanto BD_PILE + 1
    BD_PILE = 2 + SHIFT,
    Camp_Id = 4 + SHIFT,
    Power_Pulse = 5 + SHIFT, //size of 3
    Logs = 10  + SHIFT
};

enum PulseStatus : unsigned char {
    NoStatus = 0,
    Hypothesis_Type = 1,
    Hypothesis_Bisect = 2,
    Stress = 3,
    Done = 4,
};

enum PulseType : unsigned char {
    NoType = 0,
    Resistor = 1,
    Both = 2,
};

struct Pulse {  // sizeof = 3
    PulseStatus Status;
    PulseType Type;
    unsigned char Interval;
};

enum class ThroneCampType : unsigned char {
    Forest = 1,
    Farm = 2,
    Mine = 3,
};

enum class EventType : unsigned char {
    Capture = 0,
    Collect = 1,
    Loot = 2,
    Upgrade = 3,
    SetStockpile = 4,
    StartCapture = 5,
    CancelCapture = 6,
    SetStartingCollectCooldown = 7
};

struct LogEntry {
    EventType type;
    MY_PINS team;
    short heartbeat;
    short value;
};

#endif
