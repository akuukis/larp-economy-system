#ifndef MODULE_MY_HELPERS
#define MODULE_MY_HELPERS

#include <Arduino.h>

byte getSeconds(long time_ms);
byte getMinutes(long time_ms);
byte getHours(long time_ms);
void formatHMS(char* buffer, byte length, long time_ms);
void formatColons(char* buffer, byte length, long time_ms, bool sign = true);

#endif
