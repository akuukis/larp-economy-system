#line 2 "myLCDTest"

#include <Arduino.h>
#include <AUnit.h>
#include "myLCD.h"

using namespace aunit;

class myLCDFixture: public TestOnce {
    protected:
        MyLCD lcd;
};

testF(myLCDFixture, backlight) {
    assertEqual(lcd.isBacklight(), false);
    lcd.backlight();
    assertEqual(lcd.isBacklight(), true);
    lcd.backlight();
    assertEqual(lcd.isBacklight(), true);
    lcd.noBacklight();
    assertEqual(lcd.isBacklight(), false);
    lcd.noBacklight();
    assertEqual(lcd.isBacklight(), false);
    lcd.backlight();
    assertEqual(lcd.isBacklight(), true);
}

testF(myLCDFixture, clearFlush) {
    lcd.printDef("================", "================");
    lcd.flush();
    assertEqual(lcd.serizalizeScreen(), "================\n================\n");
    lcd.clear();
    assertEqual(lcd.serizalizeScreen(), "================\n================\n");
    lcd.flush();
    assertEqual(lcd.serizalizeScreen(), "                \n                \n");
}

testF(myLCDFixture, printAt) {
    lcd.printAt(0, 0, "abc");
    lcd.printAt(7, 0, "||");
    lcd.printAt(13, 0, "def");
    lcd.printAt(0, 1, "ghi");
    lcd.printAt(7, 1, "||");
    lcd.printAt(13, 1, "jkl");
    lcd.flush();
    assertEqual(lcd.serizalizeScreen(), "abc    ||    def\nghi    ||    jkl\n");
}

testF(myLCDFixture, printDef) {
    lcd.printDef("0123456789ABCDEF", "FEDCBA9876543210");
    lcd.flush();
    assertEqual(lcd.serizalizeScreen(), "0123456789ABCDEF\nFEDCBA9876543210\n");
}

//---------------------------------------------------------------------------

void setup() {
    Serial.begin(9600);
}

void loop() {
    TestRunner::run();
}
