#ifndef MODULE_MY_LCD
#define MODULE_MY_LCD

#include <LiquidCrystal_I2C.h>
#include <WString.h>
#include <Arduino.h>

/**
 * This file has helpers specifically for our LCD hardware.
 * - Model: LCD1602
 * - Description: Serial Blue Backlight LCD Module 16 X 2
 * - Link: www.aliexpress.com/item/Blue-Display-IIC-I2C-TWI-SP-I-Serial-Interface-1602-16X2-Character-LCD-Module/32278943097.html
 */

class MyLCD : public LiquidCrystal_I2C {
public:
    MyLCD();
    void printAt(byte x, byte y, const char* content = "                ");
    void printAt(byte x, byte y, String content);
    void printDef(const char* content1 = "                ", const char* content2 = "                ");
    void printDef(String content1, String content2);
    void flush();
    void clear();
    void noBacklight();
    void backlight();
    bool isBacklight();
protected:
    char _buffer[2][17];
    bool _backlightOn;
};

#endif
