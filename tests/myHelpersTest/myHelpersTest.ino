#line 2 "myHelpersTest"

#include <Arduino.h>
#include <AUnit.h>
#include "myHelpers.h"

using aunit::TestRunner;

test(myHelpersTest, getSeconds) {
    assertEqual(getSeconds(100), 0);
    assertEqual(getSeconds(1000), 1);
    assertEqual(getSeconds(59 * 1000), 59);
    assertEqual(getSeconds(60 * 1000), 0);
    assertEqual(getSeconds(61 * 1000), 1);
}

test(myHelpersTest, getMinutes) {
    assertEqual(getMinutes(1000), 0);
    assertEqual(getMinutes(60 * 1000), 1);
    assertEqual(getMinutes(2 * 60 * 1000), 2);
    assertEqual(getMinutes(10 * 60 * 1000), 10);
    assertEqual(getMinutes(100 * 60 * 1000), 40);
}

test(myHelpersTest, getHours) {
    assertEqual(getHours(1000), 0);
    assertEqual(getHours(60 * 60 * 1000), 1);
    assertEqual(getHours(2 * 60 * 60 * 1000), 2);
    assertEqual(getHours(10 * 60 * 60 * 1000), 10);
    assertEqual(getHours(100 * 60 * 60 * 1000), 100);
}

test(myHelpersTest, formatHMS) {
    const byte BUFFER_LEN = 17;
    char buffer[BUFFER_LEN];

    formatHMS(buffer, BUFFER_LEN, 0);
    assertEqual(buffer, "0s");

    formatHMS(buffer, BUFFER_LEN, 1000);
    assertEqual(buffer, "1s");

    formatHMS(buffer, BUFFER_LEN, 60*1000);
    assertEqual(buffer, "1m0s");

    formatHMS(buffer, BUFFER_LEN, (60 + 5)*1000);
    assertEqual(buffer, "1m5s");

    formatHMS(buffer, BUFFER_LEN, 3600*1000);
    assertEqual(buffer, "1h0m0s");

    formatHMS(buffer, BUFFER_LEN, (12*3600 + 24*60 + 59)*1000);
    assertEqual(buffer, "12h24m59s");

    formatHMS(buffer, BUFFER_LEN, -(3600 + 24*60 + 59)*1000);
    assertEqual(buffer, "-1h24m59s");
}

test(myHelpersTest, formatColons) {
    const byte BUFFER_LEN = 17;
    char buffer[BUFFER_LEN];

    formatColons(buffer, BUFFER_LEN, 0, false);
    assertEqual(buffer, "00:00:00");

    formatColons(buffer, BUFFER_LEN, 1000, false);
    assertEqual(buffer, "00:00:01");

    formatColons(buffer, BUFFER_LEN, 60*1000, false);
    assertEqual(buffer, "00:01:00");

    formatColons(buffer, BUFFER_LEN, (60 + 5)*1000, false);
    assertEqual(buffer, "00:01:05");

    formatColons(buffer, BUFFER_LEN, 3600*1000, false);
    assertEqual(buffer, "01:00:00");

    formatColons(buffer, BUFFER_LEN, (12*3600 + 24*60 + 59)*1000, false);
    assertEqual(buffer, "12:24:59");

    formatColons(buffer, BUFFER_LEN, (12*3600 + 24*60 + 59)*1000, true);
    assertEqual(buffer, " 12:24:59");

    formatColons(buffer, BUFFER_LEN, -(12*3600 + 24*60 + 59)*1000, true);
    assertEqual(buffer, "-12:24:59");
}

//---------------------------------------------------------------------------

void setup() {
    Serial.begin(9600);
}

void loop() {
    TestRunner::run();
}
